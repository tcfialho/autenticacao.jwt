﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Infrastructures.Extensions;
using Infrastructures;
using Autenticacao.Jwt.Domain.Services;
using Autenticacao.Jwt.Domain.Repositories;
using Autenticacao.Jwt.Domain.Results;
using Autenticacao.Jwt.Domain.Business;
using Autenticacao.Jwt.Domain.Entities;

namespace Autenticacao.Jwt.Services
{
    public class AutenticacaoService : IAutenticacaoService
    {
        private readonly IConfiguration _configuration;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IDistributedCache _distributedCache;
        private readonly IContrato _contrato;

        private const int _expirationTime = 30;

        public AutenticacaoService(IConfiguration configuration, IUsuarioRepository usuarioRepository, IDistributedCache distributedCache, IContrato contrato)
        {
            this._configuration = configuration;
            this._usuarioRepository = usuarioRepository;
            this._distributedCache = distributedCache;
            this._contrato = contrato;
        }

        public async Task<CredenciaisResult> Login(string email, string senha)
        {
            var usuario = await _usuarioRepository.ObterPorEmail(email);

            _contrato.IncluirRegras(new RegraUsuarioDeveInformarSuaSenha(usuario, senha));

            if (!_contrato.Satisfeito())
                return null;
            
            var newAcessToken = CreateAccessToken(_expirationTime, usuario);

            var newRefreshToken = await CreateRefreshToken(_expirationTime, usuario);
            
            return new CredenciaisResult() { AccessToken = newAcessToken, RefreshToken = newRefreshToken };
        }

        public async Task<CredenciaisResult> Refresh(string refreshToken)
        {
            var user = await _usuarioRepository.ObterPorRefreshToken(refreshToken);

            _contrato.IncluirRegras(new RegraUsuarioDevePossuirUmaSessao(user));
            
            if (!_contrato.Satisfeito())
                return null;

            var newAcessToken = CreateAccessToken(_expirationTime, user);

            var newRefreshToken = await CreateRefreshToken(_expirationTime, user);

            return new CredenciaisResult() { AccessToken = newAcessToken, RefreshToken = newRefreshToken };
        }

        public async Task Logout(string refreshToken)
        {
            var refreshTokenHash = refreshToken.ToSHA1Hash();
            await _distributedCache.RemoveAsync(refreshTokenHash);
        }

        private async Task<string> CreateRefreshToken(int expirationTime, Usuario usuario)
        {
            var refreshToken = Guid.NewGuid().ToString();
            var refreshTokenHash = refreshToken.ToSHA1Hash();
            await _distributedCache.SetStringAsync(refreshTokenHash, usuario.Id.ToString(), new DistributedCacheEntryOptions() { AbsoluteExpiration = DateTime.Now.AddMinutes(expirationTime * 2) });
            return refreshToken;
        }

        private string CreateAccessToken(int expirationTime, Usuario usuario)
        {
            var jwtSecretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var securityToken = new JwtSecurityToken(signingCredentials: new SigningCredentials(jwtSecretKey, SecurityAlgorithms.HmacSha256),
                                                     expires: DateTime.Now.AddMinutes(expirationTime),
                                                     claims: new[] { new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                                     new Claim(JwtRegisteredClaimNames.Sub, usuario.Id.ToString()),
                                                                     new Claim(JwtRegisteredClaimNames.Email, usuario.Email)
                                                     });
            var acessToken = new JwtSecurityTokenHandler().WriteToken(securityToken);
            return acessToken;
        }
    }
}
