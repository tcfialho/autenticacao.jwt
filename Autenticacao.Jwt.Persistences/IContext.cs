﻿using Autenticacao.Jwt.Domain.Entities;
using Infrastructures.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Autenticacao.Jwt.Persistences
{
    public interface IContext : IDatabaseContext
    {
        DbSet<Usuario> Usuarios { get; set; }
    }
}
