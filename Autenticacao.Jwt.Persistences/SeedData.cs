﻿using Autenticacao.Jwt.Domain.Entities;
using System.Linq;

namespace Autenticacao.Jwt.Persistences
{
    public static class SeedData
    {
        public static void Initialize(AutenticacaoContext context)
        {
            if (!context.Usuarios.Any())
            {
                context.Usuarios.Add(new Usuario { Nome = "Nome do Usuario de Teste", Email = "teste@teste.com", Senha = "teste" });

                context.Commit();
            }
        }
    }
}