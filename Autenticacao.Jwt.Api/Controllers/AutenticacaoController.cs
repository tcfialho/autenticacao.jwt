﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Infrastructures.Mvc;
using System.Net;
using Autenticacao.Jwt.Domain.Services;
using Autenticacao.Jwt.Domain.Results;
using Autenticacao.Jwt.Domain.Commands;

namespace Autenticacao.Jwt.Controllers
{
    [Route("api/[controller]")]
    public class AutenticacaoController : ApiController
    {
        private readonly IAutenticacaoService _autenticacaoService;

        public AutenticacaoController(IAutenticacaoService autenticacaoService,
                                      IApiNotificationHandler notifications) : base(notifications)
        {
            this._autenticacaoService = autenticacaoService;
        }

        //Login
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(CredenciaisResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] AutenticacaoCommand args)
            => await ApiResponse(_autenticacaoService.Login(args.Email, args.Senha));

        //Refresh Login
        [AllowAnonymous]
        [HttpPut("{refreshtoken}")]
        [ProducesResponseType(typeof(CredenciaisResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put(string refreshtoken)
            => await ApiResponse(_autenticacaoService.Refresh(refreshtoken));

        //Logout
        [AllowAnonymous]
        [HttpDelete("{refreshtoken}")]
        public async Task<IActionResult> Delete(string refreshtoken)
            => await ApiResponse(_autenticacaoService.Logout(refreshtoken));
    }
}