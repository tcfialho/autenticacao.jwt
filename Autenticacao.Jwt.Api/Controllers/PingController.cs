﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Autenticacao.Jwt.Controllers
{
    [Route("api/[controller]")]
    public class PingController : Controller
    {
        [Authorize]
        [HttpGet()]
        public async Task<IActionResult> Get()
        {
            await Task.Run(() => { });
            return Ok();
        }
    }

}