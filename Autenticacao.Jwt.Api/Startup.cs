﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Autenticacao.Jwt.Domain.Repositories;
using Autenticacao.Jwt.Domain.Services;
using Autenticacao.Jwt.Infrastructures.Persistences;
using Autenticacao.Jwt.Infrastructures.Repositories;
using Infrastructures;
using Infrastructures.Mvc;
using Infrastructures.Persistence;
using Infrastructures.Persistence.EntityFramework;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace Autenticacao.Jwt
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public Startup(IHostingEnvironment env, IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _env = env;
            _configuration = configuration;
            _loggerFactory = loggerFactory;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureConnection(services);

            ConfigureAuthentication(services);

            ConfigureDistributedCache(services);

            ConfigureSwagger(services);

            ConfigureMvc(services);

            services.AddSingleton(x => _configuration);

            services.AddScoped<IAutenticacaoService, AutenticacaoService>();

            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        }

        private static void ConfigureMvc(IServiceCollection services)
        {
            services.AddScoped<IContrato, Contrato>();

            services.AddScoped<IApiNotificationHandler, ApiNotificationHandler>();

            services.AddMvc(options =>
            {
                options.Filters.Add<HandlerErrorAttribute>();
                options.Filters.Add<ApiNotificationAttribute>();
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseAuthentication();
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Versioned API v1.0");
                });
            }
            app.UseAuthentication();
            app.UseMvc();
        }

        protected virtual void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0",
                    new Info
                    {
                        Title = "Autenticação JWT",
                        Version = "v1.0",
                        Contact = new Contact
                        {
                            Name = "Thiago Fialho",
                            Url = "https://bitbucket.org/tcfialho/autenticacao.jwt"
                        }
                    });

                var caminhoAplicacao = PlatformServices.Default.Application.ApplicationBasePath;
                var nomeAplicacao = PlatformServices.Default.Application.ApplicationName;
                var caminhoXmlDoc = Path.Combine(caminhoAplicacao, $"{nomeAplicacao}.xml");

                if (File.Exists(caminhoXmlDoc))
                {
                    c.IncludeXmlComments(caminhoXmlDoc);
                }

                c.DescribeAllEnumsAsStrings();

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                });
            });
        }

        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
            });
        }

        protected virtual void ConfigureConnection(IServiceCollection services)
        {
            services.AddDbContext<AutenticacaoContext>(options =>
            {
                var dataconnection = "|DataDirectory|";
                var defaultConnection = _configuration.GetConnectionString("DefaultConnection");
                var appDataPath = Path.Combine(_env.ContentRootPath, "App_Data");
                var connectionString = defaultConnection.Replace(dataconnection, appDataPath);

                options.UseSqlServer(connectionString);

                var loggerFactory = new ServiceCollection()
                    .AddLogging(builder => builder.AddDebug().AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information))
                    .BuildServiceProvider()
                    .GetService<ILoggerFactory>();

                options.UseLoggerFactory(loggerFactory);
            });
            services.AddScoped<IDatabaseContext, AutenticacaoContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        protected virtual void ConfigureDistributedCache(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
        }
    }
}
