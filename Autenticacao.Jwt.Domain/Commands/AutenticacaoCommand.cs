﻿using System.ComponentModel.DataAnnotations;

namespace Autenticacao.Jwt.Domain.Commands
{
    public class AutenticacaoCommand
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Senha { get; set; }
    }
}