﻿using Autenticacao.Jwt.Domain.Results;
using System.Threading.Tasks;

namespace Autenticacao.Jwt.Domain.Services
{
    public interface IAutenticacaoService
    {
        Task<CredenciaisResult> Login(string email, string senha);
        Task<CredenciaisResult> Refresh(string refreshToken);
        Task Logout(string refreshtoken);
    }
}
