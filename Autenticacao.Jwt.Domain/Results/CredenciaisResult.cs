﻿namespace Autenticacao.Jwt.Domain.Results
{
    public class CredenciaisResult
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
