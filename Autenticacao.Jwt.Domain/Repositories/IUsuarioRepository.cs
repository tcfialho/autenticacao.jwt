﻿using Autenticacao.Jwt.Domain.Entities;
using Infrastructures.Persistence;
using System.Threading.Tasks;

namespace Autenticacao.Jwt.Domain.Repositories
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Task<Usuario> ObterPorEmail(string email);

        Task<Usuario> ObterPorRefreshToken(string refreshToken);
    }
}
