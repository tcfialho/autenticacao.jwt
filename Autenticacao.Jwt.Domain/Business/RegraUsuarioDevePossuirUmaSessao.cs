﻿using Autenticacao.Jwt.Domain.Entities;
using Infrastructures;

namespace Autenticacao.Jwt.Domain.Business
{
    public class RegraUsuarioDevePossuirUmaSessao : RegraDeNegocio
    {
        public override int CodigoDoErro { get { return 401; } }

        public override string MensagemDeErro { get { return "Sessão expirada."; } }

        private readonly Usuario _usuario;

        public RegraUsuarioDevePossuirUmaSessao(Usuario usuario)
        {
            _usuario = usuario;
        }

        public override void Validate()
        {
            this.EhValido = _usuario != null;
        }
    }
}
