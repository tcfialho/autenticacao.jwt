﻿using Autenticacao.Jwt.Domain.Entities;
using Infrastructures;

namespace Autenticacao.Jwt.Domain.Business
{
    public class RegraUsuarioDeveInformarSuaSenha : RegraDeNegocio
    {
        public override int CodigoDoErro { get { return 400; } }

        public override string MensagemDeErro { get { return "Usuário / senha inválido."; } }

        public readonly Usuario _usuario;
        public readonly string _senha;

        public RegraUsuarioDeveInformarSuaSenha(Usuario usuario, string senha)
        {
            _usuario = usuario;
            _senha = senha;
        }

        public override void Validate()
        {
            this.EhValido = _usuario != null && _usuario.Senha == _senha;
        }
    }
}
