﻿using Infrastructures.Persistence;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Autenticacao.Jwt.Domain.Entities
{
    [Table("Usuario")]
    public class Usuario: Entity<int>
    {
        [Key]
        public override int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}