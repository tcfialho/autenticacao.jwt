﻿using System.Collections.Generic;
using Autenticacao.Jwt.Domain.Entities;
using Autenticacao.Jwt.Infrastructures.Persistences;

namespace Autenticacao.Jwt.Tests.Api
{
    public static class Utilities
    {
        public static void InitializeDbForTests(AutenticacaoContext db)
        {
            db.Usuarios.AddRange(GetUsers());
            db.Commit();
        }

        public static IEnumerable<Usuario> GetUsers()
        {
            return new List<Usuario>()
            {
                new Usuario(){ Email = "teste@teste.com", Nome = "teste", Senha="1234" },
            };
        }
    }
}
