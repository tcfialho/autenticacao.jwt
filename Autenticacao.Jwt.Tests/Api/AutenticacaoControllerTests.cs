using System;
using Xunit;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using FluentAssertions;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Autenticacao.Jwt.Domain.Commands;
using Autenticacao.Jwt.Domain.Services;
using Autenticacao.Jwt.Domain.Results;

namespace Autenticacao.Jwt.Tests.Api
{
    public class AutenticacaoControllerTests : IClassFixture<TestWebApplicationFactory<Autenticacao.Jwt.Startup>>
    {        
        private static HttpClient _client;
        private readonly TestWebApplicationFactory<Autenticacao.Jwt.Startup> _factory;
        private readonly IServiceProvider _serviceProvider;

        public AutenticacaoControllerTests(TestWebApplicationFactory<Autenticacao.Jwt.Startup> factory)
        {            
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions());
            _serviceProvider = _factory.Server.Host.Services;
        }

        [Fact]
        public async void AoLogarDeveRetornarOK()
        {           
            var usuario = Utilities.GetUsers().First();
            var login = new AutenticacaoCommand() { Email = usuario.Email, Senha = usuario.Senha };
            var content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            var message = new HttpRequestMessage(HttpMethod.Post, "/api/Autenticacao") { Content = content };

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
        }

        [Fact]
        public async void AoLogarDeveRetornarUnauthorized()
        {
            var login = new AutenticacaoCommand() { Email = "nao@utorizado.com", Senha = "naoautorizado" };
            var content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            var message = new HttpRequestMessage(HttpMethod.Post, "/api/Autenticacao") { Content = content };

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData("invalidmail", "senha")]
        [InlineData("mail@mail.com", "")]
        [InlineData("", "senha")]
        [InlineData("", "")]
        public async void AoLogarDeveRetornarBadRequest(string email, string senha)
        {
            var login = new AutenticacaoCommand() { Email = email, Senha = senha };
            var content = new StringContent(JsonConvert.SerializeObject(login, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }), Encoding.UTF8, "application/json");
            var message = new HttpRequestMessage(HttpMethod.Post, "/api/Autenticacao") { Content = content };

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void AoFazerLogoffDeveRetornarOK()
        {
            var usuario = Utilities.GetUsers().First();
            var autenticacaoService = _serviceProvider.GetService<IAutenticacaoService>();
            var autenticacao = await autenticacaoService.Login(usuario.Email, usuario.Senha);
            var message = new HttpRequestMessage(HttpMethod.Delete, $"/api/Autenticacao/{autenticacao.RefreshToken}");

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
        }

        [Fact]
        public async void AoRenovarUmTokenDeveRetornarOK()
        {
            var usuario = Utilities.GetUsers().First();
            var autenticacaoService = _serviceProvider.GetService<IAutenticacaoService>();
            var autenticacao = await autenticacaoService.Login(usuario.Email, usuario.Senha);
            var message = new HttpRequestMessage(HttpMethod.Put, $"/api/Autenticacao/{autenticacao.RefreshToken}");

            var response = await _client.SendAsync(message);

            JsonConvert.DeserializeObject<CredenciaisResult>(await response.Content.ReadAsStringAsync());

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
        }

        [Fact]
        public async void AoRenovarUmTokenInvalidoDeveRetornarUnauthorized()
        {
            var message = new HttpRequestMessage(HttpMethod.Put, $"/api/Autenticacao/tokeninvalido");

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async void AoRenovarUmTokenExpiradoDeveRetornarUnauthorized()
        {
            var message = new HttpRequestMessage(HttpMethod.Put, $"/api/Autenticacao/{Guid.NewGuid().ToString()}");

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async void AoAcessarUmEndpointRestritoSemEstarLogadoDeveRetornarUnauthorized()
        {
            var message = new HttpRequestMessage(HttpMethod.Get, "/api/ping") { };

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async void AoAcessarUmEndpointRestritoComUsuarioLogadoDeveRetornarOK()
        {
            var usuario = Utilities.GetUsers().First();
            var autenticacaoService = _serviceProvider.GetService<IAutenticacaoService>();
            var autenticacao = await autenticacaoService.Login(usuario.Email, usuario.Senha);

            var message = new HttpRequestMessage(HttpMethod.Get, "/api/ping") { };

            message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", autenticacao.AccessToken);

            var response = await _client.SendAsync(message);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
        }
    }
}
