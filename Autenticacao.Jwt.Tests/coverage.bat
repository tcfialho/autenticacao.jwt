dotnet test /p:Include="[Autenticacao.*]*" /p:Exclude="[Autenticacao.*Persistences*]*" /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput="../Coverage/coverage.xml"

reportgenerator "-reports:../coverage/coverage.xml" "-targetdir:../coverage/" -reporttypes:HTMLInline