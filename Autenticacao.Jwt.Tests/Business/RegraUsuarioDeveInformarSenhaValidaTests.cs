using Autenticacao.Jwt.Domain.Business;
using Autenticacao.Jwt.Domain.Entities;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Xunit;

namespace Autenticacao.Jwt.Unit.Tests.Business
{
    public class RegraUsuarioDeveInformarSuaSenhaTests
    {
        [Fact]
        public void TemQueSerValidoQuandoASenha�Correta()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            // Arrange
            var senha = fixture.Create(nameof(Usuario.Senha));
            var usuario = new Usuario() { Senha = senha };
            var regra = new RegraUsuarioDeveInformarSuaSenha(usuario, senha);

            // Act
            regra.Validate();

            // Assert: A regra � satisfeita
            regra.EhValido.Should().BeTrue();
        }

        [Fact]
        public void NaoTemQueSerValidoQuandoASenhaNao�Correta()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            // Arrange
            var senha = fixture.Create(nameof(Usuario.Senha));
            var outraSenha = fixture.Create(nameof(Usuario.Senha));

            var usuario = new Usuario() { Senha = senha };
            var regra = new RegraUsuarioDeveInformarSuaSenha(usuario, outraSenha);

            // Act
            regra.Validate();

            // Assert: A regra � satisfeita
            regra.EhValido.Should().BeFalse();
        }
    }
}
