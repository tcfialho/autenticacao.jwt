﻿using Autenticacao.Jwt.Domain.Business;
using Autenticacao.Jwt.Domain.Entities;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Xunit;

namespace Autenticacao.Jwt.Unit.Tests.Business
{
    public class RegraUsuarioDevePossuirUmaSessaoTests
    {
        [Fact]
        public void TemQueSerValidoQuandoPossuiUmaSessao()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            // Arrange
            var usuario = fixture.Create<Usuario>();
            var regra = new RegraUsuarioDevePossuirUmaSessao(usuario);

            // Act
            regra.Validate();

            // Assert: A regra é satisfeita
            regra.EhValido.Should().BeTrue();
        }

        [Fact]
        public void NaoTemQueSerValidoQuandoNaoPossuiUmaSessao()
        {
            // Arrange
            var regra = new RegraUsuarioDevePossuirUmaSessao(null);

            // Act
            regra.Validate();

            // Assert: A regra é satisfeita
            regra.EhValido.Should().BeFalse();
        }
    }
}
