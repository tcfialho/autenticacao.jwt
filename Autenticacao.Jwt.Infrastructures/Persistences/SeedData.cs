﻿using System.Linq;
using Autenticacao.Jwt.Domain.Entities;

namespace Autenticacao.Jwt.Infrastructures.Persistences
{
    public static class SeedData
    {
        public static void Initialize(AutenticacaoContext context)
        {
            if (!context.Usuarios.Any())
            {
                context.Usuarios.Add(new Usuario { Nome = "Nome do Usuario de Teste", Email = "teste@teste.com", Senha = "teste" });

                context.Commit();
            }
        }
    }
}