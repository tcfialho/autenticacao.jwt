﻿using Autenticacao.Jwt.Domain.Entities;
using Infrastructures.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Autenticacao.Jwt.Infrastructures.Persistences
{
    public class AutenticacaoContext : Context, IContext
    {
        public DbSet<Usuario> Usuarios { get; set; }

        public AutenticacaoContext(DbContextOptions<AutenticacaoContext> options) : base(options)
        {
        }
    }
}
