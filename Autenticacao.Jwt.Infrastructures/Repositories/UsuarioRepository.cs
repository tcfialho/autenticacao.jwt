﻿using System.Threading.Tasks;
using Autenticacao.Jwt.Domain.Entities;
using Autenticacao.Jwt.Domain.Repositories;
using Infrastructures.Extensions;
using Infrastructures.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;

namespace Autenticacao.Jwt.Infrastructures.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        private readonly IDistributedCache _cache;

        public UsuarioRepository(IDatabaseContext db, IDistributedCache cache) : base(db)
        {
            _cache = cache;
        }

        public async Task<Usuario> ObterPorEmail(string email)
        {
            return await Set().FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<Usuario> ObterPorRefreshToken(string refreshToken)
        {
            var refreshTokenHash = refreshToken.ToSHA1Hash();

            int.TryParse(await _cache.GetStringAsync(refreshTokenHash), out int userId);

            return await Obter(userId);
        }
    }
}
